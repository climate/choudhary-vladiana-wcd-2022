import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import metpy.calc
from metpy.units import units
from netCDF4 import num2date
import numpy as np
import xarray as xr
import pickle


############################################22sept#################################

data=xr.open_dataset('other/ap2pl_input1.nc')
data=data.sel(plev=85000)
data = data.metpy.parse_cf()
data.QV.attrs['units'] = 'kg/kg'
data['QV']=data['QV']/1000
data['air_pressure'] = data['air_pressure']/100
data.air_pressure.attrs['units'] = 'hPa'

dewpoint=metpy.calc.dewpoint_from_specific_humidity(data['air_pressure'], data['T'], data['QV'])
ap2pl_thetae=metpy.calc.equivalent_potential_temperature(data['air_pressure'], data['T'], dewpoint)
ap2pl_thetae=np.array(ap2pl_thetae)

with open("ap2pl_thetae22.pkl","wb") as f:
    pickle.dump(ap2pl_thetae,f)

###################################23sept##############################################

data=xr.open_dataset('other/ap2pl_input2.nc')
data=data.sel(plev=85000)
data = data.metpy.parse_cf()
data.QV.attrs['units'] = 'kg/kg'
data['QV']=data['QV']/1000
data['air_pressure'] = data['air_pressure']/100
data.air_pressure.attrs['units'] = 'hPa'

dewpoint=metpy.calc.dewpoint_from_specific_humidity(data['air_pressure'], data['T'], data['QV'])
ap2pl_thetae=metpy.calc.equivalent_potential_temperature(data['air_pressure'], data['T'], dewpoint)
ap2pl_thetae=np.array(ap2pl_thetae)

with open("ap2pl_thetae23.pkl","wb") as f:
    pickle.dump(ap2pl_thetae,f)

####################################24sept###############################################


data=xr.open_dataset('other/ap2pl_input3.nc')
data=data.sel(plev=85000)
data = data.metpy.parse_cf()
data.QV.attrs['units'] = 'kg/kg'
data['QV']=data['QV']/1000
data['air_pressure'] = data['air_pressure']/100
data.air_pressure.attrs['units'] = 'hPa'

dewpoint=metpy.calc.dewpoint_from_specific_humidity(data['air_pressure'], data['T'], data['QV'])
ap2pl_thetae=metpy.calc.equivalent_potential_temperature(data['air_pressure'], data['T'], dewpoint)
ap2pl_thetae=np.array(ap2pl_thetae)

with open("ap2pl_thetae24.pkl","wb") as f:
    pickle.dump(ap2pl_thetae,f)
