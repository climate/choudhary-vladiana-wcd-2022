#!/bin/bash
#BATCH --job-name=ipv_batch
#SBATCH --partition=prepost,gpu
#SBATCH --ntasks=48
#SBATCH --mem-per-cpu=5300
#SBATCH --time=12:00:00
#SBATCH --account=bm0834
#SBATCH --exclusive
#SBATCH --output=ipv_batch.o%j

set -evx
echo "start: $(date)"

export CDO_NTHREADS=12
export OMP_NUM_THREADS=${CDO_NTHREADS}
srun_cmd="srun -Q -n1 -c${CDO_NTHREADS} --cpu-bind=threads --exclusive"

module load nco
ncatted -a standard_name,P,o,c,"air_pressure" ../P20160922_12 _P20160922_12
ncatted -a standard_name,P,o,c,"air_pressure" ../P20160923_12 _P20160923_12
ncatted -a standard_name,P,o,c,"air_pressure" ../P20160924_12 _P20160924_12

mv _P20160922_12 P20160922_12
mv _P20160923_12 P20160923_12
mv _P20160924_12 P20160924_12

#ncatted -O standard_name,PS,o,c,"surface pressure" P20160922_12
#ncatted -O standard_name,PS,o,c,"surface pressure" P20160923_12              
#ncatted -O standard_name,PS,o,c,"surface pressure" P20160924_12


cdo -P ${CDO_NTHREADS} ap2plx,100000,92500,85000,60000,50000,40000,35000,30000,25000,20000,15000,10000,5000 -sellonlatbox,-60,30,25,75 -aexpr,'air_pressure=air_pressure*100'  -aexpr,'PS=PS*100' -chname,P,air_pressure -selvar,P,PS,QV,T,U,V,z_mc P20160922_12 input1.nc

cdo -P ${CDO_NTHREADS} ap2plx,100000,92500,85000,60000,50000,40000,35000,30000,25000,20000,15000,10000,5000 -sellonlatbox,-60,30,25,75 -aexpr,'air_pressure=air_pressure*100'  -aexpr,'PS=PS*100' -chname,P,air_pressure -selvar,P,PS,QV,T,U,V,z_mc P20160923_12 input2.nc

cdo -P ${CDO_NTHREADS} ap2plx,100000,92500,85000,60000,50000,40000,35000,30000,25000,20000,15000,10000,5000 -sellonlatbox,-60,30,25,75 -aexpr,'air_pressure=air_pressure*100'  -aexpr,'PS=PS*100' -chname,P,air_pressure -selvar,P,PS,QV,T,U,V,z_mc P20160924_12 input3.nc



echo "end: $(date)"

exit


