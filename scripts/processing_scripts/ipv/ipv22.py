#!/usr/bin/env python
# coding: utf-8

# In[38]:


import numpy as np
import numpy.ma as ma
import netCDF4
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from scipy.ndimage import gaussian_filter
from cartopy.feature import NaturalEarthFeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from datetime import datetime
import cartopy.feature as cfeature
import cartopy.util as cutil
import scipy.ndimage as ndimage
from pylab import text
import xarray as xr
import pickle


# In[61]:


# VALUES TO SET *************************************************
# set date, lat-lon range, and PV-value definition of tropopause
mydate='20160922'
myhour='12'
sel_tim = 0      # timestep after initialization (note that in GFS 0.25 is 3h)
(lat1,lat2)=(25,75)
(lon1,lon2)=(-60,29.975)
tpdef=2          # definition of tropopause in PVU
#****************************************************************
#lon1=lon1+360
#lon2=lon2+360


# In[62]:


# some constants
re=6.37e6
g=9.81
cp=1004.5
r=2*cp/7
kap=r/cp
omega=7.292e-5
pi=3.14159265


# In[82]:


# open dataset, retreive variables, close dataset
# analysis
#url='http://nomads.ncep.noaa.gov:80/dods/gfs_0p25/gfs'+\
#mydate+'/gfs_0p25_'+myhour+'z_anl'

# forecast
#url='http://nomads.ncep.noaa.gov:80/dods/gfs_0p25/gfs'+\
#mydate+'/gfs_0p25_'+myhour+'z'

file = netCDF4.Dataset('input1.nc')
lat_in  = file.variables['lat'][:]
lon_in  = file.variables['lon'][:]
lev     = (file.variables['plev'][:])/100
time_in = file.variables['time'][:]
pres2pv_in = (file.variables['PS'][sel_tim,:,:,:])
t_in = file.variables['T'][sel_tim,:,:,:]
u_in = file.variables['U'][sel_tim,:,:,:]
v_in = file.variables['V'][sel_tim,:,:,:]
hgt_in = file.variables['z_mc'][sel_tim,:,:,:]
file.close()

print('####################')
print('')
print(' Start calculations ')
print('')
print('####################')


# In[71]:


# get array indices for lat-lon range
# specified above
iy1 = np.argmin( np.abs( lat_in - lat1 ) )
iy2 = np.argmin( np.abs( lat_in - lat2 ) ) 
ix1 = np.argmin( np.abs( lon_in - lon1 ) )
ix2 = np.argmin( np.abs( lon_in - lon2 ) )  


# In[101]:


# select specified lat-lon range
t=t_in[:,iy1:iy2,ix1:ix2]
lon=lon_in[ix1:ix2]
lat=lat_in[iy1:iy2]
u=u_in[:,iy1:iy2,ix1:ix2]
v=v_in[:,iy1:iy2,ix1:ix2]
hgt=hgt_in[:,iy1:iy2,ix1:ix2]
pres2pv=pres2pv_in[:,iy1:iy2,ix1:ix2]


# In[108]:


# some prep work for derivatives
xlon,ylat=np.meshgrid(lon,lat)
dlony,dlonx=np.gradient(xlon)
dlaty,dlatx=np.gradient(ylat)
dx=re*np.cos(ylat*pi/180)*dlonx*pi/180
dy=re*dlaty*pi/180


# In[122]:


# define potential temperature and Coriolis parameter
theta=t*(1.E5/(lev[:,np.newaxis,np.newaxis]*100))**kap
f=2*omega*np.sin(ylat*pi/180)


# In[112]:


# calculate derivatives
# (np.gradient can handle 1D uneven spacing,
# so build that in for p, but do dx and dy 
# external to the function since they are 2D)
ddp_theta=np.gradient(theta,lev*100,axis=0)
ddx_theta=np.gradient(theta,axis=2)/dx
ddy_theta=np.gradient(theta,axis=1)/dy
ddp_u=np.gradient(u,lev*100,axis=0)
ddp_v=np.gradient(v,lev*100,axis=0)
ddx_v=np.gradient(v,axis=2)/dx
ddy_ucos=np.gradient(u*np.cos(ylat*pi/180),axis=1)/dy


# In[11]:


# calculate contributions to PV and PV
absvort=ddx_v-(1/np.cos(ylat*pi/180))*ddy_ucos+f
pv_one=g*absvort*(-ddp_theta)
pv_two=g*(ddp_v*ddx_theta-ddp_u*ddy_theta)
pv=pv_one+pv_two


# In[12]:


# Calculate pressure of tropopause as well as potential temperature (theta) and height
#
# starting from 100hPa and working down, to avoid more complicated vertical structure higher up
nx=ix2-ix1+1
ny=iy2-iy1+1
nz=lev.size
nzs=np.argwhere(lev==100)[0,0]
tp=np.empty((ny-1,nx-1))*np.nan   # initialize as undef
tp_theta=np.empty((ny-1,nx-1))*np.nan   # initialize as undef
tp_hgt=np.empty((ny-1,nx-1))*np.nan   # initialize as undef

for ix in range(0,nx-1):
    for iy in range(0,ny-1):
        for iz in range(nzs,0,-1):
            if pv[iz,iy,ix]/1e-6<=tpdef:
                if np.isnan(tp[iy,ix]):
                    tp[iy,ix]=(
                    (lev[iz]*(pv[iz+1,iy,ix]-tpdef*1e-6)
                    -lev[iz+1]*(pv[iz,iy,ix]-tpdef*1e-6))/
                    (pv[iz+1,iy,ix]-pv[iz,iy,ix])
                    )
    
                    tp_theta[iy,ix]=(
                    ((lev[iz]-tp[iy,ix])*theta[iz+1,iy,ix]+
                    (tp[iy,ix]-lev[iz+1])*theta[iz,iy,ix])/
                    (lev[iz]-lev[iz+1])
                    )
                    
                    tp_hgt[iy,ix]=(
                    ((lev[iz]-tp[iy,ix])*hgt[iz+1,iy,ix]+
                    (tp[iy,ix]-lev[iz+1])*hgt[iz,iy,ix])/
                    (lev[iz]-lev[iz+1])
                    )


# In[ ]:


# calculate PV on the 320K isentropic surface
# (also not in a pythonic way)
nx=ix2-ix1+1
ny=iy2-iy1+1
nz=lev.size
pv320=np.empty((ny-1,nx-1))*np.nan   # initialize as undef
for ix in range(0,nx-1):
    for iy in range(0,ny-1):
        for iz in range(nz-2,0,-1):
            if theta[iz,iy,ix]>=320:
                if theta[iz-1,iy,ix]<=320:
                    if np.isnan(pv320[iy,ix]):
                        pv320[iy,ix]=(
                        ((320-theta[iz-1,iy,ix])*pv[iz,iy,ix]+
                        (theta[iz,iy,ix]-320)*pv[iz-1,iy,ix])/
                        (theta[iz,iy,ix]-theta[iz-1,iy,ix])
                        )


# In[ ]:


# we smooth the data
pv320=gaussian_filter(pv320,sigma=1)

with open("ipv22.pkl","wb") as f:
    pickle.dump(pv320,f)

# define spatial correlation function for testing results
#def scorr(a,b):
#    abar=np.mean(a)
#    bbar=np.mean(b)
#    covar=sum((a-abar)*(b-bbar))
#    avar=sum((a-abar)**2)
#    bvar=sum((b-bbar)**2)
#    r=covar/np.sqrt(avar*bvar)
#    return(r)


# In[ ]:


# identify latitude of lowest tropopause (there are some issues with this one)
maxloc=np.argwhere(tp==np.amax(tp))
latmax=lat[maxloc[0,0]]

with open("latmax.pkl","wb") as f:
    pickle.dump(latmax,f)


# In[ ]:


#%%
print('####################')
print('')
print('    Start plots     ')
print('')
print('####################')

#states_provinces = cfeature.NaturalEarthFeature(
#            category='cultural',
#           name='admin_1_states_provinces_lines',
#           scale='50m',
#           facecolor='none')
#    y tates = NaturalEarthFeature(category='cultural', 
#    scale='110m', facecolor='none', 
#    name='admin_0_countries')
fdate=datetime.strptime(mydate, '%Y%m%d').strftime('%d %b %Y')

#pv320B , cycl_lon  = cutil.add_cyclic_point(pv320,coord=lon)
#hgt_inB, cycl_lon2 = cutil.add_cyclic_point(hgt_in,coord=lon_in)

plt.figure(figsize=(13, 13))
plt.rcParams.update({'font.size': 20})
#ax = plt.axes(projection=ccrs.NearsidePerspective(central_latitude=40,central_longitude=15,satellite_height=15785831 ))
ax = plt.subplot(1,1,1,projection=ccrs.PlateCarree()) #PlateCarree() #NearsidePerspective(central_longitude=25.0, central_latitude=50.0, satellite_height=35785831, false_easting=0, false_northing=0, globe=None)

ax.set_xticks([-50,-40,-30,-20,-10,0,10,20], crs=ccrs.PlateCarree())
ax.set_xticklabels(['50W','40W','30W','20W','10W','EQ','10E','20E'])
ax.set_yticks([30,40,50,60,70], crs=ccrs.PlateCarree())
ax.set_yticklabels(['30N','40N','50N','60N','70N'])
ax.set_extent([-50, 20, 30, 70])
ax.coastlines()

#ax.set_global()
#ax.add_feature(states_provinces,linewidth=0.9, edgecolor='k')
#ax.add_feature(cfeature.BORDERS,linewidth=0.9, linestyle='--', alpha=0.3)
#ax.add_feature(cfeature.LAND.with_scale('50m'),facecolor='dimgrey',alpha=0.3) #land_alt1
#ax.coastlines('50m', linewidth=0.8,color='k')
#clevs2 = np.arange(-0.5,9,12)
clevs2 = [-0.5,0.2,0.7,1,1.5,2.0,4,5,6,7,8,9,10]
cl     = plt.contour( lon,lat,pv320/1e-6,clevs2,colors='gray',linewidths=0.3,extend='both',transform=ccrs.PlateCarree())
cp     = plt.contourf(lon,lat,pv320/1e-6,clevs2,cmap='Spectral_r', extend='both',transform=ccrs.PlateCarree()) #RdBu_r

#cbar   = plt.colorbar(cp, orientation='horizontal',fraction=0.05, pad=0.04)
#cbar.set_label('PVU',fontsize=12)
#bounds = ['-0.5','0.2','0.7','1','1.5','2','4','5','6','7','8','9','10']
#cbar.set_ticklabels(bounds)
#cbar.ax.tick_params(labelsize=12)

#ds=xr.open_dataset('other/slp23t12')

#clevs = np.arange(960,1040,4)
#pres_msl=ds.pres_msl.squeeze('time')
#cs = plt.contour(lon,lat,pres_msl/100,clevs, colors='brown',linewidths=1.)
#plt.clabel(cs, fmt='%d', fontsize=9, inline=1)
#clevs3=np.arange(480,600,10)
#cs = plt.contour(lon,lat,hgt[3,:,:]/10,clevs3,transform=ccrs.PlateCarree(),colors='k',linewidths=1)
#clabels = plt.clabel(cs,inline=1, fmt='%1.0f',ticks=clevs3, colors=('k',), fontsize=10)
#[txt.set_bbox(dict(facecolor='white', edgecolor='none', pad=0)) for txt in clabels]
#for l in clabels:
#    l.set_rotation(0)
#gl = ax.gridlines(linewidth=1.9,color='k')
#gl.xlabels_bottom = gl.ylabels_left = True
#gl.xformatter = LONGITUDE_FORMATTER
#gl.yformatter = LATITUDE_FORMATTER
#gl.xlocator = mticker.FixedLocator(np.arange(-180.25,179.25,180))
#gl.ylocator = mticker.FixedLocator(np.arange(0,90.1,90))
#barbspace=25
#ax.barbs(lon[::barbspace],lat[::barbspace],1.943846*np.nanmean(u[16:18,::barbspace,::barbspace],0),1.943846*np.nanmean(v[16:18,::barbspace,::barbspace],0),length=5,
#         sizes=dict(emptybarb=.1, spacing=.2, height=.5,facecolor='white'),
#         linewidth=0.7,color='k', transform=ccrs.PlateCarree())
plt.title('(a) 2016-09-22T12:00:00',fontsize=24,weight='bold')
#ax.annotate(
#    'Plot by Georgios Papavasileiou (KIT) \n georgios.papavasileiou@kit.edu', 
#    (0, 1),
#    xytext=(4, -4),
#    xycoords='axes fraction',
#    textcoords='offset points',
#    fontweight='bold',
#    color='white',
#    backgroundcolor='k',
#    ha='left', va='top')
#ax.annotate(
#    '[GFS] Initialized: '+myhour+'Z '+fdate, 
#    (0,0.054),
#    xytext=(4, -4),
#    xycoords='axes fraction',
#    textcoords='offset points',
#    fontweight='bold',
#    color='white',
#    backgroundcolor='k',
#    ha='left', va='top')
#ax.annotate(
#    '[GFS] Analysis: '+myhour+'Z '+fdate, 
#    (0,0.054),
#    xytext=(4, -4),
#    xycoords='axes fraction',
#    textcoords='offset points',
#    fontweight='bold',
#    color='white',
#    backgroundcolor='k',
#    ha='left', va='top')
#plt.show()
plt.savefig('PV_320K_22T12.png', dpi=600)
#fig1.savefig('PV_320K_and500hPaGeopot_'+str(myhour)+'Z_'+str(fdate)+'.png',dpi=300,bbox_inches='tight')
