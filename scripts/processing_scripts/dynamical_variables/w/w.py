#!/usr/bin/env python
# coding: utf-8

# In[1]:


import xarray as xr
import pickle
from scipy import stats
from scipy import *
import matplotlib.pyplot as plt
import numpy as np
import warnings
warnings.simplefilter("ignore")


# In[16]:


########alltraj##################################################

ds1=xr.open_dataset('../../../0.8/1m/outtrace2.nc')
ds2=xr.open_dataset('../../../0.4/1m/outtrace2.nc')
ds3=xr.open_dataset('../../../0.2/1m/outtrace2.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/outtrace2.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/outtrace2.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/outtrace2.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/outtrace2.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/outtrace2.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/outtrace2.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.W>=-200) & (ds1.W<=200)), other=0)
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.W[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.W>=-200) & (ds2.W<=200)), other=0)
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.W[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.W>=-200) & (ds3.W<=200)), other=0)
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.W[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.W>=-200) & (ds4.W<=200)), other=0)
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.W[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.W>=-200) & (ds5.W<=200)), other=0)
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.W[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.W>=-200) & (ds6.W<=200)), other=0)
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.W[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.W>=-200) & (ds7.W<=200)), other=0)
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.W[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.W>=-200) & (ds8.W<=200)), other=0)
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.W[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.W>=-200) & (ds9.W<=200)), other=0)
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.W[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=bin_means1
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=bin_means2
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=bin_means3
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=bin_means4
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=bin_means5
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=bin_means6
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=bin_means7
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=bin_means8
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=bin_means9
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("binmeanalltraj.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9

########################################traj1#########################################################


ds1=xr.open_dataset('../../../0.8/1m/outtrace2box1.nc')
ds2=xr.open_dataset('../../../0.4/1m/outtrace2box1.nc')
ds3=xr.open_dataset('../../../0.2/1m/outtrace2box1.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/outtrace2box1.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/outtrace2box1.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/outtrace2box1.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/outtrace2box1.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/outtrace2box1.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/outtrace2box1.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.W>=-200) & (ds1.W<=200)), other=0)
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.W[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.W>=-200) & (ds2.W<=200)), other=0)
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.W[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.W>=-200) & (ds3.W<=200)), other=0)
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.W[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.W>=-200) & (ds4.W<=200)), other=0)
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.W[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.W>=-200) & (ds5.W<=200)), other=0)
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.W[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.W>=-200) & (ds6.W<=200)), other=0)
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.W[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.W>=-200) & (ds7.W<=200)), other=0)
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.W[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.W>=-200) & (ds8.W<=200)), other=0)
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.W[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.W>=-200) & (ds9.W<=200)), other=0)
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.W[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=bin_means1
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=bin_means2
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=bin_means3
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=bin_means4
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=bin_means5
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=bin_means6
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=bin_means7
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=bin_means8
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=bin_means9
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("binmeantraj1.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9

#####################################traj2###########################################################


ds1=xr.open_dataset('../../../0.8/1m/outtrace2box2.nc')
ds2=xr.open_dataset('../../../0.4/1m/outtrace2box2.nc')
ds3=xr.open_dataset('../../../0.2/1m/outtrace2box2.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/outtrace2box2.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/outtrace2box2.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/outtrace2box2.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/outtrace2box2.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/outtrace2box2.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/outtrace2box2.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.W>=-200) & (ds1.W<=200)), other=0)
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.W[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.W>=-200) & (ds2.W<=200)), other=0)
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.W[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.W>=-200) & (ds3.W<=200)), other=0)
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.W[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.W>=-200) & (ds4.W<=200)), other=0)
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.W[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.W>=-200) & (ds5.W<=200)), other=0)
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.W[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.W>=-200) & (ds6.W<=200)), other=0)
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.W[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.W>=-200) & (ds7.W<=200)), other=0)
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.W[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.W>=-200) & (ds8.W<=200)), other=0)
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.W[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.W>=-200) & (ds9.W<=200)), other=0)
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.W[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=bin_means1
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=bin_means2
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=bin_means3
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=bin_means4
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=bin_means5
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=bin_means6
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=bin_means7
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=bin_means8
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=bin_means9
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("binmeantraj2.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9

###################################traj3##############################################


ds1=xr.open_dataset('../../../0.8/1m/outtrace2box3.nc')
ds2=xr.open_dataset('../../../0.4/1m/outtrace2box3.nc')
ds3=xr.open_dataset('../../../0.2/1m/outtrace2box3.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/outtrace2box3.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/outtrace2box3.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/outtrace2box3.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/outtrace2box3.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/outtrace2box3.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/outtrace2box3.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.W>=-200) & (ds1.W<=200)), other=0)
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.W[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.W>=-200) & (ds2.W<=200)), other=0)
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.W[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.W>=-200) & (ds3.W<=200)), other=0)
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.W[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.W>=-200) & (ds4.W<=200)), other=0)
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.W[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.W>=-200) & (ds5.W<=200)), other=0)
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.W[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.W>=-200) & (ds6.W<=200)), other=0)
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.W[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.W>=-200) & (ds7.W<=200)), other=0)
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.W[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.W>=-200) & (ds8.W<=200)), other=0)
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.W[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.W>=-200) & (ds9.W<=200)), other=0)
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.W[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=bin_means1
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=bin_means2
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=bin_means3
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=bin_means4
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=bin_means5
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=bin_means6
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=bin_means7
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=bin_means8
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=bin_means9
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("binmeantraj3.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9


############################################traj4###################################


#ds1=xr.open_dataset('../../../0.8/1m/outtrace2box4.nc')
ds2=xr.open_dataset('../../../0.4/1m/outtrace2box4.nc')
ds3=xr.open_dataset('../../../0.2/1m/outtrace2box4.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/outtrace2box4.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/outtrace2box4.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/outtrace2box4.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/outtrace2box4.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/outtrace2box4.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/outtrace2box4.nc')


ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.W>=-200) & (ds2.W<=200)), other=0)
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.W[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.W>=-200) & (ds3.W<=200)), other=0)
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.W[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.W>=-200) & (ds4.W<=200)), other=0)
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.W[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.W>=-200) & (ds5.W<=200)), other=0)
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.W[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.W>=-200) & (ds6.W<=200)), other=0)
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.W[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.W>=-200) & (ds7.W<=200)), other=0)
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.W[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.W>=-200) & (ds8.W<=200)), other=0)
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.W[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.W>=-200) & (ds9.W<=200)), other=0)
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.W[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))



y2=bin_means2
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=bin_means3
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=bin_means4
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=bin_means5
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=bin_means6
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=bin_means7
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=bin_means8
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=bin_means9
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("binmeantraj4.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9

#########################################################################
