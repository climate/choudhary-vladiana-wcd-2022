# coding: utf-8

# In[1]:

import xarray as xr
import glob
import os
import time
import sys


filename = sorted(glob.glob('P2016*'))


# In[258]:

for x in range(1):
    filename1 = sys.argv[1]
    #ofilename = filename1 + '.nc4'
    ofilename=filename1.replace("P", "S")
    print('Input file:', filename1, 'Output file:', ofilename)
    
    tt=time.process_time()
    
    ds=xr.open_dataset(filename1)


    u=ds.U
    v=ds.V
    lon=ds.lon
    lat=ds.lat
    dvdx = v.differentiate('lon')*180/3.1416
    dudy = (u*xr.ufuncs.cos(lat*3.1416/180)).differentiate('lat')*180/3.1416

    relvort=(dvdx-dudy)/(6.37e6*xr.ufuncs.cos(lat*3.1416/180))
    f=0.00014544*xr.ufuncs.sin(lat*0.017453292)
    AV=relvort+f
    AV=AV.rename('AV')
    del u, v, lon, lat, relvort, f




    
    print('Time for calculation of whole script:', time.process_time() - tt)


