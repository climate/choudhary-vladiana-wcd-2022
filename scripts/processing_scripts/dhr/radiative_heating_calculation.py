#!/usr/bin/env python
# coding: utf-8

# In[1]:


import xarray as xr
import numpy as np
import glob
import os
import time
import sys


# In[108]:


filename = sorted(glob.glob('P2016*'))


# In[258]:

for x in range(1):
    filename1 = sys.argv[1]
    #ofilename = filename1 + '.nc4'
    ofilename=filename1.replace("P", "S")
    print('Input file:', filename1, 'Output file:', ofilename)
    
    tt=time.process_time()
    
    ds=xr.open_dataset(filename1)
    
    
    g      = 9.81
    cv_dry = 717.6
    pdiff=-ds['P'].diff('height', label='lower')*100
    flx=(ds['LWFLXALL'] - ds['LWFLXCLR']) + (ds['SWFLXALL']- ds['SWFLXCLR'])
    flxlw=ds['LWFLXALL']
    flxsw=ds['SWFLXALL']
    flxlwnet=ds['LWFLXALL']- ds['LWFLXCLR']
    flxswnet=ds['SWFLXALL']- ds['SWFLXCLR']

    flx=flx.rename('flx')
    flxdiff=flx.diff('height', label='lower')
    hr=+86400 * (g/cv_dry) * (flxdiff/pdiff)
    hr=hr.rename('hr')


    flxlwdiff=flxlw.diff('height', label='lower')
    hrlw=+86400 * (g/cv_dry) * (flxlwdiff/pdiff)
    hrlw=hrlw.rename('hrlw')

    flxswdiff=flxsw.diff('height', label='lower')
    hrsw=+86400 * (g/cv_dry) * (flxswdiff/pdiff)
    hrsw=hrsw.rename('hrsw')

    flxlwnetdiff=flxlwnet.diff('height', label='lower')
    hrlwnet=+86400 * (g/cv_dry) * (flxlwnetdiff/pdiff)
    hrlwnet=hrlwnet.rename('hrlwnet')

    flxswnetdiff=flxswnet.diff('height', label='lower')
    hrswnet=+86400 * (g/cv_dry) * (flxswnetdiff/pdiff)
    hrswnet=hrswnet.rename('hrswnet')

    
    #merging of all calculations
    
    final= xr.merge([flx, hr, hrlw, hrsw, hrlwnet, hrswnet])
    
    path = ofilename
    if os.path.exists(path):
        os.remove(path)

    #ds.to_netcdf(path=ofilename, format='NETCDF4_CLASSIC')
    final.to_netcdf(path=ofilename, engine='netcdf4')
    
    print('Time for calculation of whole script:', time.process_time() - tt)
