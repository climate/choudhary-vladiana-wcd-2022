import xarray as xr
import pickle
from scipy import stats
from scipy import *
import matplotlib.pyplot as plt
import numpy as np
import warnings
warnings.simplefilter("ignore")
######################################################################
ds1=xr.open_dataset('../../../0.8/1m/crh/outtrace4.nc')
ds2=xr.open_dataset('../../../0.4/1m/crh/outtrace4.nc')
ds3=xr.open_dataset('../../../0.2/1m/crh/outtrace4.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/crh/outtrace4.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/crh/outtrace4.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/crh/outtrace4.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/crh/outtrace4.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/crh/outtrace4.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/crh/outtrace4.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.hr>=-200) & (ds1.hr<=200)))
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.hr[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.hr>=-200) & (ds2.hr<=200)))
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.hr[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.hr>=-200) & (ds3.hr<=200)))
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.hr[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.hr>=-200) & (ds4.hr<=200)))
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.hr[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.hr>=-200) & (ds5.hr<=200)))
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.hr[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.hr>=-200) & (ds6.hr<=200)))
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.hr[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.hr>=-200) & (ds7.hr<=200)))
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.hr[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.hr>=-200) & (ds8.hr<=200)))
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.hr[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.hr>=-200) & (ds9.hr<=200)))
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.hr[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=np.nanmean(bin_means1, axis=1)
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=np.nanmean(bin_means2, axis=1)
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=np.nanmean(bin_means3, axis=1)
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=np.nanmean(bin_means4, axis=1)
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=np.nanmean(bin_means5, axis=1)
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=np.nanmean(bin_means6, axis=1)
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=np.nanmean(bin_means7, axis=1)
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=np.nanmean(bin_means8, axis=1)
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=np.nanmean(bin_means9, axis=1)
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("alltrajhrnew.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9
###################################################################
ds1=xr.open_dataset('../../../0.8/1m/crh/outtrace4.nc')
ds2=xr.open_dataset('../../../0.4/1m/crh/outtrace4.nc')
ds3=xr.open_dataset('../../../0.2/1m/crh/outtrace4.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/crh/outtrace4.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/crh/outtrace4.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/crh/outtrace4.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/crh/outtrace4.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/crh/outtrace4.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/crh/outtrace4.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.hrlw>=-200) & (ds1.hrlw<=200)))
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.hrlw[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.hrlw>=-200) & (ds2.hrlw<=200)))
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.hrlw[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.hrlw>=-200) & (ds3.hrlw<=200)))
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.hrlw[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.hrlw>=-200) & (ds4.hrlw<=200)))
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.hrlw[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.hrlw>=-200) & (ds5.hrlw<=200)))
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.hrlw[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.hrlw>=-200) & (ds6.hrlw<=200)))
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.hrlw[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.hrlw>=-200) & (ds7.hrlw<=200)))
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.hrlw[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.hrlw>=-200) & (ds8.hrlw<=200)))
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.hrlw[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.hrlw>=-200) & (ds9.hrlw<=200)))
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.hrlw[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=np.nanmean(bin_means1, axis=1)
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=np.nanmean(bin_means2, axis=1)
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=np.nanmean(bin_means3, axis=1)
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=np.nanmean(bin_means4, axis=1)
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=np.nanmean(bin_means5, axis=1)
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=np.nanmean(bin_means6, axis=1)
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=np.nanmean(bin_means7, axis=1)
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=np.nanmean(bin_means8, axis=1)
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=np.nanmean(bin_means9, axis=1)
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("alltrajhrlw.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9

#########################################################################################################################
ds1=xr.open_dataset('../../../0.8/1m/crh/outtrace4.nc')
ds2=xr.open_dataset('../../../0.4/1m/crh/outtrace4.nc')
ds3=xr.open_dataset('../../../0.2/1m/crh/outtrace4.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/crh/outtrace4.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/crh/outtrace4.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/crh/outtrace4.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/crh/outtrace4.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/crh/outtrace4.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/crh/outtrace4.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.hrsw>=-200) & (ds1.hrsw<=200)))
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.hrsw[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.hrsw>=-200) & (ds2.hrsw<=200)))
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.hrsw[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.hrsw>=-200) & (ds3.hrsw<=200)))
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.hrsw[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.hrsw>=-200) & (ds4.hrsw<=200)))
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.hrsw[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.hrsw>=-200) & (ds5.hrsw<=200)))
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.hrsw[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.hrsw>=-200) & (ds6.hrsw<=200)))
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.hrsw[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.hrsw>=-200) & (ds7.hrsw<=200)))
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.hrsw[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.hrsw>=-200) & (ds8.hrsw<=200)))
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.hrsw[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.hrsw>=-200) & (ds9.hrsw<=200)))
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.hrsw[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=np.nanmean(bin_means1, axis=1)
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=np.nanmean(bin_means2, axis=1)
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=np.nanmean(bin_means3, axis=1)
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=np.nanmean(bin_means4, axis=1)
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=np.nanmean(bin_means5, axis=1)
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=np.nanmean(bin_means6, axis=1)
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=np.nanmean(bin_means7, axis=1)
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=np.nanmean(bin_means8, axis=1)
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=np.nanmean(bin_means9, axis=1)
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("alltrajhrsw.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9

###################################################################################################

ds1=xr.open_dataset('../../../0.8/1m/crh/outtrace4.nc')
ds2=xr.open_dataset('../../../0.4/1m/crh/outtrace4.nc')
ds3=xr.open_dataset('../../../0.2/1m/crh/outtrace4.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/crh/outtrace4.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/crh/outtrace4.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/crh/outtrace4.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/crh/outtrace4.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/crh/outtrace4.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/crh/outtrace4.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.hrswnet>=-20) & (ds1.hrswnet<=20)))
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.hrswnet[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.hrswnet>=-20) & (ds2.hrswnet<=20)))
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.hrswnet[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.hrswnet>=-20) & (ds3.hrswnet<=20)))
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.hrswnet[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.hrswnet>=-20) & (ds4.hrswnet<=20)))
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.hrswnet[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.hrswnet>=-20) & (ds5.hrswnet<=20)))
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.hrswnet[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.hrswnet>=-20) & (ds6.hrswnet<=20)))
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.hrswnet[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.hrswnet>=-20) & (ds7.hrswnet<=20)))
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.hrswnet[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.hrswnet>=-20) & (ds8.hrswnet<=20)))
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.hrswnet[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.hrswnet>=-20) & (ds9.hrswnet<=20)))
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.hrswnet[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=np.nanmean(bin_means1, axis=1)
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=np.nanmean(bin_means2, axis=1)
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=np.nanmean(bin_means3, axis=1)
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=np.nanmean(bin_means4, axis=1)
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=np.nanmean(bin_means5, axis=1)
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=np.nanmean(bin_means6, axis=1)
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=np.nanmean(bin_means7, axis=1)
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=np.nanmean(bin_means8, axis=1)
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=np.nanmean(bin_means9, axis=1)
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("alltrajhrswnet.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9
##########################################################################################

ds1=xr.open_dataset('../../../0.8/1m/crh/outtrace4.nc')
ds2=xr.open_dataset('../../../0.4/1m/crh/outtrace4.nc')
ds3=xr.open_dataset('../../../0.2/1m/crh/outtrace4.nc')
ds4=xr.open_dataset('../../../0.1convon/1m/crh/outtrace4.nc')
ds5=xr.open_dataset('../../../0.05convon/1m/crh/outtrace4.nc')
ds6=xr.open_dataset('../../../0.025convon/1m/crh/outtrace4.nc')
ds7=xr.open_dataset('../../../0.1convoff/1m/crh/outtrace4.nc')
ds8=xr.open_dataset('../../../0.05convoff/1m/crh/outtrace4.nc')
ds9=xr.open_dataset('../../../0.025convoff/1m/crh/outtrace4.nc')

ntim,ntra=ds1.P.shape
nbins=10
ds1=ds1.where(((ds1.hrlwnet>=-200) & (ds1.hrlwnet<=200)))
bin_means1 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds1.P[:,i].squeeze()
    dpvr_tot1=ds1.hrlwnet[:,i].squeeze()
    bin_means1[:, i], bin_edges1, binnumber1= stats.binned_statistic(P, dpvr_tot1, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds2.P.shape
nbins=10
ds2=ds2.where(((ds2.hrlwnet>=-200) & (ds2.hrlwnet<=200)))
bin_means2 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds2.P[:,i].squeeze()
    dpvr_tot2=ds2.hrlwnet[:,i].squeeze()
    bin_means2[:, i], bin_edges2, binnumber2= stats.binned_statistic(P, dpvr_tot2, mean, bins=nbins, range=(200,1050))

ntim,ntra=ds3.P.shape
nbins=10
ds3=ds3.where(((ds3.hrlwnet>=-200) & (ds3.hrlwnet<=200)))
bin_means3 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds3.P[:,i].squeeze()
    dpvr_tot3=ds3.hrlwnet[:,i].squeeze()
    bin_means3[:, i], bin_edges3, binnumber3= stats.binned_statistic(P, dpvr_tot3, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds4.P.shape
nbins=10
ds4=ds4.where(((ds4.hrlwnet>=-200) & (ds4.hrlwnet<=200)))
bin_means4 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds4.P[:,i].squeeze()
    dpvr_tot4=ds4.hrlwnet[:,i].squeeze()
    bin_means4[:, i], bin_edges4, binnumber4= stats.binned_statistic(P, dpvr_tot4, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds5.P.shape
nbins=10
ds5=ds5.where(((ds5.hrlwnet>=-200) & (ds5.hrlwnet<=200)))
bin_means5 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds5.P[:,i].squeeze()
    dpvr_tot5=ds5.hrlwnet[:,i].squeeze()
    bin_means5[:, i], bin_edges5, binnumber5= stats.binned_statistic(P, dpvr_tot5, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds6.P.shape
nbins=10
ds6=ds6.where(((ds6.hrlwnet>=-200) & (ds6.hrlwnet<=200)))
bin_means6 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds6.P[:,i].squeeze()
    dpvr_tot6=ds6.hrlwnet[:,i].squeeze()
    bin_means6[:, i], bin_edges6, binnumber6= stats.binned_statistic(P, dpvr_tot6, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds7.P.shape
nbins=10
ds7=ds7.where(((ds7.hrlwnet>=-200) & (ds7.hrlwnet<=200)))
bin_means7 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds7.P[:,i].squeeze()
    dpvr_tot7=ds7.hrlwnet[:,i].squeeze()
    bin_means7[:, i], bin_edges7, binnumber7= stats.binned_statistic(P, dpvr_tot7, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds8.P.shape
nbins=10
ds8=ds8.where(((ds8.hrlwnet>=-200) & (ds8.hrlwnet<=200)))
bin_means8 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds8.P[:,i].squeeze()
    dpvr_tot8=ds8.hrlwnet[:,i].squeeze()
    bin_means8[:, i], bin_edges8, binnumber8= stats.binned_statistic(P, dpvr_tot8, mean, bins=nbins, range=(200,1050))
    
ntim,ntra=ds9.P.shape
nbins=10
ds9=ds9.where(((ds9.hrlwnet>=-200) & (ds9.hrlwnet<=200)))
bin_means9 = np.full((nbins, ntra), np.nan)
for i in range(ntra):
    P=ds9.P[:,i].squeeze()
    dpvr_tot9=ds9.hrlwnet[:,i].squeeze()
    bin_means9[:, i], bin_edges9, binnumber9= stats.binned_statistic(P, dpvr_tot9, mean, bins=nbins, range=(200,1050))


y1=np.nanmean(bin_means1, axis=1)
x1=bin_edges1[:-1] + np.diff(bin_edges1)/2

y2=np.nanmean(bin_means2, axis=1)
x2=bin_edges2[:-1] + np.diff(bin_edges2)/2

y3=np.nanmean(bin_means3, axis=1)
x3=bin_edges3[:-1] + np.diff(bin_edges3)/2

y4=np.nanmean(bin_means4, axis=1)
x4=bin_edges4[:-1] + np.diff(bin_edges4)/2

y5=np.nanmean(bin_means5, axis=1)
x5=bin_edges5[:-1] + np.diff(bin_edges5)/2

y6=np.nanmean(bin_means6, axis=1)
x6=bin_edges6[:-1] + np.diff(bin_edges6)/2

y7=np.nanmean(bin_means7, axis=1)
x7=bin_edges7[:-1] + np.diff(bin_edges7)/2

y8=np.nanmean(bin_means8, axis=1)
x8=bin_edges8[:-1] + np.diff(bin_edges8)/2

y9=np.nanmean(bin_means9, axis=1)
x9=bin_edges9[:-1] + np.diff(bin_edges9)/2

dtot= ((x1,y1), (x2,y2), (x3,y3), (x4,y4), (x5,y5), (x6,y6), (x7,y7), (x8,y8), (x9,y9))

with open("alltrajhrlwnet.pkl","wb") as f:
    pickle.dump(dtot,f)


del ds1,ds2,ds3,ds4, ds5, ds6, ds7, ds8, ds9

############################################################################
