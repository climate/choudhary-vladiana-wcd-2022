# Analysis scripts for "Impact of grid spacing, convective parameterization and cloud microphysics in ICON simulations of a warm conveyor belt"

Code repository for python scripts used in the analysis for "Impact of grid spacing, convective parameterization and cloud microphysics in ICON simulations of a warm conveyor belt" submitted to the Weather and Climate Dynamics

The associated data for the analysis is preliminarily available here, ultimately it will be available via https://doi.org/10.5281/zenodo.5921126.
